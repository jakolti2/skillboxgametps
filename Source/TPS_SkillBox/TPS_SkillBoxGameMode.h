// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TPS_SkillBoxGameMode.generated.h"

UCLASS(minimalapi)
class ATPS_SkillBoxGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATPS_SkillBoxGameMode();
};



