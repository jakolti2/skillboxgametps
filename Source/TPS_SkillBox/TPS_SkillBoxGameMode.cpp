// Copyright Epic Games, Inc. All Rights Reserved.

#include "TPS_SkillBoxGameMode.h"
#include "TPS_SkillBoxPlayerController.h"
#include "TPS_SkillBoxCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATPS_SkillBoxGameMode::ATPS_SkillBoxGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATPS_SkillBoxPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}