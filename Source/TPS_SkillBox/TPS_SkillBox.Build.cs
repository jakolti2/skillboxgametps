// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TPS_SkillBox : ModuleRules
{
	public TPS_SkillBox(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule" });
    }
}
