// Copyright Epic Games, Inc. All Rights Reserved.

#include "TPS_SkillBox.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TPS_SkillBox, "TPS_SkillBox" );

DEFINE_LOG_CATEGORY(LogTPS_SkillBox)
 